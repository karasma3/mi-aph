import {PIXICmp} from "../../ts/engine/PIXIObject";
import Component from "../../ts/engine/Component";
import {TAG_HIPPO} from "./constants";
import {ATTR_FACTORY} from "./constants";
import HippoFactory from "./HippoFactory";

export class EnvironmentObjectComponent extends Component{

    player : PIXICmp.ComponentObject;

    onInit(){
        super.onInit();
        this.player = this.scene.findFirstObjectByTag(TAG_HIPPO);
    }
    onUpdate(delta, absolute){
        let environmentObj = this.owner.getPixiObj();
        if(environmentObj.children.length==0) {
            if (Math.abs(environmentObj.position.x - this.player.getPixiObj().position.x) < 10 &&
                Math.abs(environmentObj.position.y - this.player.getPixiObj().position.y) < 10) {
                this.scene.getGlobalAttribute<HippoFactory>(ATTR_FACTORY).creteEnvironmentEnemy(this.owner);
            }
        }
    }
}