import {SOUND_PAN_HIT, SOUND_PAN_SWING, MSG_PAN_HIT, MSG_PAN_SWING} from "./constants";
import {GenericComponent} from "../../ts/components/GenericComponent";

export class SoundComponent extends GenericComponent {
    constructor() {
        super(SoundComponent.name);

        this.doOnMessage(MSG_PAN_SWING, (cmp, msg)=> (<any>PIXI.loader.resources[SOUND_PAN_SWING]).sound.play());
        this.doOnMessage(MSG_PAN_HIT, ((cmp, msg) => (<any>PIXI.loader.resources[SOUND_PAN_HIT]).sound.play()));
    }
}