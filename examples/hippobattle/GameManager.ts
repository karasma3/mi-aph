import Component from "../../ts/engine/Component";
import {ATTR_FACTORY, ATTR_MODEL, MSG_GAME_OVER} from "./constants";
import Msg from "../../ts/engine/Msg";
import {HippoModel} from "./HippoModel";
import HippoFactory from "./HippoFactory";

export class GameManager extends Component{
    model: HippoModel;
    factory: HippoFactory;

    onInit() {
        this.model = this.scene.getGlobalAttribute<HippoModel>(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute<HippoFactory>(ATTR_FACTORY);
        super.onInit();
        this.subscribe(MSG_GAME_OVER);
    }
    onMessage(msg:Msg){
        if (msg.action ==MSG_GAME_OVER){
            this.model.isGameOver = true;
            this.scene.invokeWithDelay(5000, () => {
                this.factory.resetGame(this.scene);
            });
        }
    }
}