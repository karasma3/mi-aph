import Component from "../../ts/engine/Component";
import {PIXICmp} from "../../ts/engine/PIXIObject";
import {MSG_OBJECT_ADDED, MSG_OBJECT_REMOVED} from "../../ts/engine/Constants";
import Msg from "../../ts/engine/Msg";
import {FLAG_COLLIDABLE, FLAG_PROJECTILE} from "./constants";
import {MSG_COLLISION} from "./constants";
import * as PIXI from "pixi.js";

export class CollisionInfo {
    // hit unit
    unit: PIXICmp.ComponentObject;
    // projectile that hit given unit
    projectile: PIXICmp.ComponentObject;

    constructor(unit: PIXICmp.ComponentObject, projectile: PIXICmp.ComponentObject) {
        this.unit = unit;
        this.projectile = projectile;
    }
}

export class CollisionManager extends Component{
    units = new Array<PIXICmp.ComponentObject>();
    projectiles = new Array<PIXICmp.ComponentObject>();

    onInit() {
        super.onInit();
        this.subscribe(MSG_OBJECT_ADDED, MSG_OBJECT_REMOVED);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_OBJECT_ADDED || msg.action == MSG_OBJECT_REMOVED) {
            // refresh collections
            this.projectiles = this.scene.findAllObjectsByFlag(FLAG_PROJECTILE);
            this.units = this.scene.findAllObjectsByFlag(FLAG_COLLIDABLE);
        }
    }

    onUpdate(delta, absolute){
        let collides = new Array<CollisionInfo>();
        for (let projectile of this.projectiles) {
                for (let unit of this.units) {
                        let boundsA = projectile.getPixiObj().getBounds();
                        let boundsB = unit.getPixiObj().getBounds();

                        let intersectionX = this.testHorizIntersection(boundsA, boundsB);
                        let intersectionY = this.testVertIntersection(boundsA, boundsB);

                        if (intersectionX > 0 && intersectionY > 0) {
                            collides.push(new CollisionInfo(unit, projectile));
                        }
            }
        }
        for (let collid of collides) {
            this.sendMessage(MSG_COLLISION, collid);
        }
    }

    private testHorizIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.right, boundsB.right) - Math.max(boundsA.left, boundsB.left);
    }

    private testVertIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.bottom, boundsB.bottom) - Math.max(boundsA.top, boundsB.top);
    }

}