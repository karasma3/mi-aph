import Component from "../../ts/engine/Component";
import {
    MSG_COLLISION,
    TAG_CROCODILE,
    TAG_HIPPO,
    MSG_GAME_OVER,
    MSG_ENVIRONMENTENEMY_DIED,
    TAG_NATIVE, TAG_MONKEY,
    MSG_RESTART_HIPPO
} from "./constants";
import Msg from "../../ts/engine/Msg";
import {CollisionInfo} from "./CollisionManager";
import {HippoModel} from "./HippoModel";
import HippoFactory from "./HippoFactory";
import {ATTR_FACTORY, ATTR_MODEL} from "./constants";

export class CollisionResolver extends Component{
    model: HippoModel;
    factory: HippoFactory;

    onInit() {
        this.model = this.scene.getGlobalAttribute<HippoModel>(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute<HippoFactory>(ATTR_FACTORY);
        super.onInit();
        this.subscribe(MSG_COLLISION);
    }
    onMessage(msg: Msg) {
        if (msg.action == MSG_COLLISION) {
            this.handleCollision(msg);
        }
    }
    protected handleCollision(msg: Msg) {
        let trigger = <CollisionInfo>msg.data;

        if (trigger.unit.getTag() == TAG_CROCODILE ||
            trigger.unit.getTag() == TAG_NATIVE ||
            trigger.unit.getTag() == TAG_MONKEY){
            this.model.score += 1;
            this.sendMessage(MSG_ENVIRONMENTENEMY_DIED, trigger.unit);
        }else if(trigger.unit.getTag() == TAG_HIPPO){
            this.model.score -= 1;
            this.sendMessage(MSG_RESTART_HIPPO);
            this.sendMessage(MSG_ENVIRONMENTENEMY_DIED, trigger.projectile.getTag());
        }


        trigger.projectile.remove();
    }
}