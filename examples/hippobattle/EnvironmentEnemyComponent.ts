import Component from "../../ts/engine/Component";
import {
    ATTR_MODEL,
    MSG_ENVIRONMENTENEMY_DIED,
    TAG_ARROW,
    TAG_BANANA,
    TAG_CROCODILE,
    TAG_MONKEY,
    TAG_NATIVE
} from "./constants";
import HippoFactory from "./HippoFactory";
import {ATTR_FACTORY} from "./constants";
import {checkTime} from "./Utils";
import {MSG_PROJECTILE_SHOT} from "./constants";
import {HippoModel} from "./HippoModel";
import Vec2 from "../../ts/utils/Vec2";
import Msg from "../../ts/engine/Msg";
import {CollisionInfo} from "./CollisionManager";


export class EnvironmentEnemyComponent extends Component{

    protected lastShot = 0;
    factory: HippoFactory;
    model: HippoModel;


    onInit(){
        this.model = this.scene.getGlobalAttribute<HippoModel>(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute<HippoFactory>(ATTR_FACTORY);
        this.subscribe(MSG_ENVIRONMENTENEMY_DIED);
        this.attack();
    }

    onMessage(msg:Msg){
        if(msg.action == MSG_ENVIRONMENTENEMY_DIED){
            if(this.owner.getTag()== TAG_MONKEY && msg.data == TAG_BANANA){
                this.owner.remove();
            }else if(this.owner.getTag()== TAG_NATIVE && msg.data == TAG_ARROW){
                this.owner.remove();
            }
        }
    }

    tryFire(absolute: number): boolean {
        if (checkTime(this.lastShot, absolute, this.model.enemyFireRate)) {
            this.lastShot = absolute;
            this.factory.createProjectile(this.owner, this.model,
                new Vec2(Math.cos(this.owner.getPixiObj().rotation), Math.sin(this.owner.getPixiObj().rotation)));
            this.sendMessage(MSG_PROJECTILE_SHOT);
            return true;
        } else {
            return false;
        }
    }
    attack(){
        switch(this.owner.getTag()){
            case TAG_CROCODILE:{
                //TODO ATTACK
                break;
            }
            default: {
                this.factory.createProjectile(this.owner, this.model,
                    new Vec2(Math.cos(this.owner.getPixiObj().rotation), Math.sin(this.owner.getPixiObj().rotation)));
                this.sendMessage(MSG_PROJECTILE_SHOT);
                break;
            }
        }
    }
}