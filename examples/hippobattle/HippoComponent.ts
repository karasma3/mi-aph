import Component from "../../ts/engine/Component";
import {PIXICmp} from "../../ts/engine/PIXIObject";
import {HippoModel} from "./HippoModel";
import {MSG_PAN_SWING, TAG_GRASS, ATTR_MODEL} from "./constants";
import {KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_X, KeyInputComponent} from "../../ts/components/KeyInputComponent";
import {checkTime} from "./Utils";


export class HippoComponent extends Component {

    lastSwing = 0;
    swingRate = 50;

    move(direction: number, delta: number){
        let pixiObj = this.owner.getPixiObj();

        switch(direction) {
            case KEY_LEFT:{
                pixiObj.position.x -= delta * 0.02;
                pixiObj.rotation = 0;
                break;
            }
            case KEY_RIGHT:{
                pixiObj.position.x += delta * 0.02;
                pixiObj.rotation = Math.PI;
                break;
            }
            case KEY_UP:{
                pixiObj.position.y -= delta * 0.02;
                pixiObj.rotation = Math.PI * 0.5;
                break;
            }
            case KEY_DOWN:{
                pixiObj.position.y += delta * 0.02;
                pixiObj.rotation = Math.PI * -0.5;
                break;
            }
        }

    }
    onUpdate(delta: number, absolute: number) {
        let cmp = this.scene.stage.findComponentByClass(KeyInputComponent.name);
        let cmpKey = <KeyInputComponent><any>cmp;

        if (cmpKey.isKeyPressed(KEY_LEFT)) {
            this.move(KEY_LEFT, delta);
        }

        if (cmpKey.isKeyPressed(KEY_RIGHT)) {
            this.move(KEY_RIGHT, delta);
        }

        if (cmpKey.isKeyPressed(KEY_UP)) {
            this.move(KEY_UP, delta);
        }

        if (cmpKey.isKeyPressed(KEY_DOWN)) {
            this.move(KEY_DOWN, delta);
        }
        if (cmpKey.isKeyPressed(KEY_X)) {
            this.sendMessage(MSG_PAN_SWING);
            // this.trySwing(absolute);
        }
    }

    trySwing(absolute: number){
        if(checkTime(absolute, this.lastSwing, this.swingRate)){
            this.lastSwing = absolute;
            this.sendMessage(MSG_PAN_SWING);
            return true;
        } else {
            return false;
        }
    }

}