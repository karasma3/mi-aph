
// message keys
export const MSG_UNIT_KILLED = "unit_killed";
export const MSG_PAN_HIT = "pan_hit";
export const MSG_PAN_SWING = "pan_swing";
export const MSG_COLLISION = "collision";
export const MSG_PROJECTILE_SHOT = "projectile_shot";
export const MSG_GAME_OVER = "game_over";
export const MSG_ENVIRONMENTENEMY_DIED = "environmentenemy_died";
export const MSG_RESTART_HIPPO = "restart_hippo";

// attribute keys
export const ATTR_MODEL = "model";
export const ATTR_FACTORY = "factory";

// flags
export const FLAG_PROJECTILE = 1;
export const FLAG_COLLIDABLE = 2;

// tags for game objects
export const TAG_GRASS = "grass";
export const TAG_HIPPO = "hippo";
export const TAG_TREE = "tree";
export const TAG_MONKEY = "monkey";
export const TAG_ROCK = "rock";
export const TAG_WATER = "water";
export const TAG_CROCODILE = "crocodile";
export const TAG_HERO = "hero";
export const TAG_NATIVE = "native";
export const TAG_ARROW = "arrow";
export const TAG_BANANA = "banana";

// parachute states


// alias for config file
export const DATA_JSON = "DATA_JSON";

// texture aliases
export const TEXTURE_GRASS = "grass";
export const TEXTURE_HIPPO = "hippo";
export const TEXTURE_MONKEY = "monkey";
export const TEXTURE_CROCODILE = "crocodile";
export const TEXTURE_HERO = "hero";
export const TEXTURE_LION = "lion";
export const TEXTURE_ELEPHANT = "elephant";
export const TEXTURE_PARROT = "parrot";
export const TEXTURE_ROCK = "rock";
export const TEXTURE_TREE = "tree";
export const TEXTURE_BANANA = "banana";
export const TEXTURE_ARROW = "arrow";
export const TEXTURE_PAN = "pan";
export const TEXTURE_WATER = "water";
export const TEXTURE_NATIVE = "native";

// sound aliases
export const SOUND_PAN_HIT = "pan_hit";
export const SOUND_PAN_SWING = "pan_swing";

// height of the scene will be set to 50 units for the purpose of better calculations
export const SCENE_HEIGHT = 50;

// native height of the game canvas. If bigger, it will be resized accordingly
export const SPRITES_RESOLUTION_HEIGHT = 400;

// native speed of the game
export const GAME_SPEED = 1;