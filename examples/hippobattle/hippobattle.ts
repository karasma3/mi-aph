
import { PixiRunner } from '../../ts/PixiRunner'
import HippoFactory from './HippoFactory';
import {
    ATTR_FACTORY,
    SPRITES_RESOLUTION_HEIGHT,
    DATA_JSON,
    SCENE_HEIGHT,
    TEXTURE_GRASS,
    TAG_GRASS,
    TAG_HIPPO,
    TEXTURE_HIPPO,
    SOUND_PAN_HIT,
    SOUND_PAN_SWING,
    TEXTURE_ARROW,
    TEXTURE_BANANA,
    TEXTURE_CROCODILE,
    TEXTURE_ELEPHANT,
    TEXTURE_HERO,
    TEXTURE_LION,
    TEXTURE_MONKEY,
    TEXTURE_PAN,
    TEXTURE_PARROT,
    TEXTURE_ROCK,
    TEXTURE_TREE,
    TEXTURE_WATER,
    TEXTURE_NATIVE
} from './Constants';


class HippoBattle {
    engine: PixiRunner;
    container: PIXI.Container;

    // Start a new game
    constructor() {
        this.engine = new PixiRunner();

        let canvas = (document.getElementById("gameCanvas") as HTMLCanvasElement);

        let screenHeight = canvas.height;
        
        // calculate ratio between intended resolution (here 400px of height) and real resolution
        // - this will set appropriate scale 
        let gameScale = SPRITES_RESOLUTION_HEIGHT / screenHeight;
        // scale the scene to 50 units if height
        let resolution = screenHeight / SCENE_HEIGHT * gameScale;
        this.engine.init(canvas, resolution / gameScale);


        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TEXTURE_GRASS, 'static/hippobattle/1.jpg')
            .add(TEXTURE_HIPPO, 'static/hippobattle/hippo/hippo00001.png')
            .add(TEXTURE_MONKEY, 'static/hippobattle/monkey/monkey_fight00001.png')
            .add(TEXTURE_CROCODILE, 'static/hippobattle/crocodile/crocodile_swim00001.png')
            .add(TEXTURE_HERO, 'static/hippobattle/hero/sit/hero_sit00021.png')
            .add(TEXTURE_LION, 'static/hippobattle/lion/lion_walk00001.png')
            .add(TEXTURE_ELEPHANT, 'static/hippobattle/elephant/0020.png')
            .add(TEXTURE_PARROT, 'static/hippobattle/parrot/parrot_fight00001.png')
            .add(TEXTURE_NATIVE, 'static/hippobattle/native/nativeattack00001.png')
            .add(TEXTURE_ROCK, 'static/hippobattle/Rock Pile.png')
            .add(TEXTURE_TREE, 'static/hippobattle/Krook Tree.png')
            .add(TEXTURE_WATER, 'static/hippobattle/Water 1.jpg')
            .add(TEXTURE_BANANA, 'static/hippobattle/spinning_banana.png')
            .add(TEXTURE_ARROW, 'static/hippobattle/Arrow.png')
            .add(TEXTURE_PAN, 'static/hippobattle/pan.png')
            .add(SOUND_PAN_HIT, 'static/hippobattle/sound/FryingPanHit.mp3')
            .add(SOUND_PAN_SWING, 'static/hippobattle/sound/FryingPanSwing.mp3')
            .load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        // this.container = new PIXI.Container();
        //this.engine.scene.stage.getPixiObj().addChild(this.container);
        // let hero = PIXI.Sprite.fromImage('static/hippobattle/hero/sit/hero_sit00021.png');
        // let hippo = PIXI.Sprite.fromImage('static/hippobattle/hippo/hippo00001.png');
        // this.container.addChild(hero);
        // this.container.addChild(hippo);
        //let banana = PIXI.Texture.fromImage('static/hippobattle/spinning_banana.png');
        //banana.frame = new PIXI.Rectangle(0,0,50,25);
        let factory = new HippoFactory();
        factory.resetGame(this.engine.scene);
    }
}

new HippoBattle();

