import {DynamicsComponent} from "../../ts/components/DynamicsComponent";
import {Point} from "pixi.js";
import {MSG_OBJECT_ADDED} from "../../ts/engine/Constants";
import {MSG_ENVIRONMENTENEMY_DIED} from "./constants";


export class ProjectileComponent extends DynamicsComponent {

    onUpdate(delta, absolute){
        super.onUpdate(delta, absolute);

        // check boundaries
        let globalPos = this.owner.getPixiObj().toGlobal(new Point(0, 0));
        if (globalPos.x < 0 || globalPos.x > this.scene.app.screen.width || globalPos.y < 0 || globalPos.y > this.scene.app.screen.height) {
            this.owner.remove();
            this.sendMessage(MSG_ENVIRONMENTENEMY_DIED, this.owner.getTag());
        }
    }
}