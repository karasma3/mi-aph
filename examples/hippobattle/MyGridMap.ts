import {AStarSearch} from "../../ts/utils/Pathfinding";
import {GridMap, MAP_TYPE_TILE} from "../../ts/utils/GridMap";
import Vec2 from "../../ts/utils/Vec2";

export class MyGridMap{

    map: GridMap;
    mapCellSize: number;

    constructor(size:number){
        this.mapCellSize = size;
        let canvas = document.getElementById("gameCanvas") as HTMLCanvasElement
        let width = canvas.width / this.mapCellSize;
        let height = canvas.height / this.mapCellSize;
        this.map = new GridMap(MAP_TYPE_TILE, 10, width, height);
    }

    getNeighbors(position: Vec2){
        return this.map.getNeighbors(position);
    }

    /**
     * Transforms map coordinates into world coordinates
     */
    mapToWorld(x: number, y: number) {
        return new Vec2(x * this.mapCellSize, y * this.mapCellSize);
    }

    /**
     * Transforms world coordinates into map coordinates
     */
    worldToMap(x: number, y: number) {
        return new Vec2(Math.floor(x / this.mapCellSize), Math.floor(y / this.mapCellSize));
    }

    /**
     * Gets map index by coordinate
     */
    mapIndexByCoord(x: number, y: number) {
        return y * this.map.width + x;
    }

    /**
     * Gets map coordinate by index
     */
    mapCoordByIndex(index: number) {
        return new Vec2(index % this.map.width, Math.floor(index / this.map.width));
    }

    getRandomEmptyTile():Vec2{
        let position = new Vec2(0,0);
         while(true){
            position.x = Math.floor(Math.random() * this.map.width);
            position.y = Math.floor(Math.random() * this.map.height);
            if(!this.map.hasObstruction(position)){
                break;
            }
        }
        return position;
    }

    addObstruction(position: Vec2){
        this.map.addObstruction(position);
    }

}