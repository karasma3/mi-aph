import {
    TEXTURE_GRASS,
    TAG_GRASS,
    TAG_HIPPO,
    TAG_TREE,
    TAG_MONKEY,
    TAG_ROCK,
    TAG_WATER,
    TAG_HERO,
    TAG_CROCODILE,
    TAG_NATIVE,
    TEXTURE_HIPPO,
    TEXTURE_ARROW,
    MSG_UNIT_KILLED,
    ATTR_FACTORY,
    ATTR_MODEL,
    TEXTURE_TREE,
    TEXTURE_MONKEY,
    TEXTURE_ROCK,
    TEXTURE_WATER,
    TEXTURE_HERO,
    TEXTURE_CROCODILE,
    TEXTURE_NATIVE,
    FLAG_COLLIDABLE, TEXTURE_BANANA, FLAG_PROJECTILE
} from './constants';
import { HippoModel } from './HippoModel';
import { PIXICmp } from '../../ts/engine/PIXIObject';
import PIXIObjectBuilder from '../../ts/engine/PIXIObjectBuilder';
import { KeyInputComponent } from '../../ts/components/KeyInputComponent';
import Scene from '../../ts/engine/Scene';
import DebugComponent from '../../ts/components/DebugComponent';
import { GenericComponent } from '../../ts/components/GenericComponent';
import ChainingComponent from '../../ts/components/ChainingComponent';
import {SoundComponent} from "./SoundComponent";
import {HippoComponent} from "./HippoComponent";
import {EnvironmentObjectComponent} from "./EnvironmentObjectComponent";
import {EnvironmentEnemyComponent} from "./EnvironmentEnemyComponent";
import {TAG_BANANA, TAG_ARROW} from "./constants";
import {ProjectileComponent} from "./ProjectileComponent";
import {ATTR_DYNAMICS} from "../../ts/engine/Constants";
import Dynamics from "../../ts/utils/Dynamics";
import Vec2 from "../../ts/utils/Vec2";
import {CollisionManager} from "./CollisionManager";
import {CollisionResolver} from "./CollisionResolver";
import {GameManager} from "./GameManager";

export default class HippoFactory {

    initializeGame(rootObject: PIXICmp.ComponentObject, model: HippoModel) {

        let scene = rootObject.getScene();
        let builder = new PIXIObjectBuilder(scene);


        // add root components
        builder
            .withComponent(new KeyInputComponent())
            .withComponent(new DebugComponent(document.getElementById("debugSect")))
            .withComponent(new SoundComponent())
            .withComponent(new CollisionManager())
            .withComponent(new CollisionResolver())
            .withComponent(new GameManager())
            .build(rootObject);

        //hippo + player
        builder.relativePos(0.5, 0.5)
            .scale(0.05)
            .anchor(0.5, 0.5)
            .withFlag(FLAG_COLLIDABLE)
            .withComponent(new HippoComponent)
            .build(new PIXICmp.Sprite(TAG_HIPPO, PIXI.Texture.fromImage(TEXTURE_HIPPO)), rootObject);

        this.createEnvironment(scene, builder, rootObject, model);
    }
    createEnvironment(scene, builder: PIXIObjectBuilder, rootObject, model){
        //tree
        builder.relativePos(Math.random(), Math.random())
            .scale(0.015)
            .anchor(0.5, 0.5)
            .withComponent(new EnvironmentObjectComponent())
            .build(new PIXICmp.Sprite(TAG_TREE, PIXI.Texture.fromImage(TEXTURE_TREE)), rootObject);
        //rock
        builder.relativePos(Math.random(), Math.random())
            .scale(0.015)
            .anchor(0.5, 0.5)
            .withComponent(new EnvironmentObjectComponent())
            .build(new PIXICmp.Sprite(TAG_ROCK, PIXI.Texture.fromImage(TEXTURE_ROCK)), rootObject);
        //water
        builder.relativePos(Math.random(), Math.random())
            .scale(0.1)
            .anchor(0.5, 0.5)
            .withComponent(new EnvironmentObjectComponent())
            .build(new PIXICmp.Sprite(TAG_WATER, PIXI.Texture.fromImage(TEXTURE_WATER)), rootObject);
    }

    creteEnvironmentEnemy(environmentObj:PIXICmp.ComponentObject){
        let builder = new PIXIObjectBuilder(environmentObj.getScene());
        switch (environmentObj.getTag()) {
            //monkey
            case TAG_TREE:{
                builder.globalPos(environmentObj.getPixiObj().position.x,environmentObj.getPixiObj().position.y-3)
                    .scale(5)
                    .anchor(0.5, 0.5)
                    .withComponent(new EnvironmentEnemyComponent())
                    .build(new PIXICmp.Sprite(TAG_MONKEY, PIXI.Texture.fromImage(TEXTURE_MONKEY)), environmentObj);
                break;
            }
            //native
            case TAG_ROCK:{
                builder.globalPos(environmentObj.getPixiObj().position.x - 2,environmentObj.getPixiObj().position.y-1)
                    .scale(2)
                    .anchor(0.5, 0.5)
                    .withComponent(new EnvironmentEnemyComponent())
                    .build(new PIXICmp.Sprite(TAG_NATIVE, PIXI.Texture.fromImage(TEXTURE_NATIVE)), environmentObj);
                break;
            }
            //crocodile
            case TAG_WATER:{
                builder.globalPos(environmentObj.getPixiObj().position.x,environmentObj.getPixiObj().position.y - 5)
                    .scale(0.5)
                    .anchor(0.5, 0.5)
                    .withComponent(new EnvironmentEnemyComponent())
                    .build(new PIXICmp.Sprite(TAG_CROCODILE, PIXI.Texture.fromImage(TEXTURE_CROCODILE)), environmentObj);
                break;
            }
        }
    }

    createProjectile(environmentEnemy: PIXICmp.ComponentObject, model: HippoModel, direction: Vec2){
        let builder = new PIXIObjectBuilder(environmentEnemy.getScene());
        let dynamics = new Dynamics();
        dynamics.velocity = direction.multiply(150);
        let projectile;
        if(environmentEnemy.getTag() == TAG_MONKEY){
            projectile = new PIXICmp.Sprite(TAG_BANANA, PIXI.Texture.fromImage(TEXTURE_BANANA));
        }else{
            projectile = new PIXICmp.Sprite(TAG_ARROW, PIXI.Texture.fromImage(TEXTURE_ARROW));
        }
        builder.globalPos(environmentEnemy.getPixiObj().toGlobal(new PIXI.Point(0, 0)).x,environmentEnemy.getPixiObj().toGlobal(new PIXI.Point(0, 0)).y)
            .scale(1)
            .anchor(0.5, 0.5)
            .withAttribute(ATTR_DYNAMICS, dynamics)
            .withFlag(FLAG_PROJECTILE)
            .withComponent(new ProjectileComponent())
            .build(projectile, environmentEnemy);
    }

    resetGame(scene: Scene) {
        scene.clearScene();
        let model = new HippoModel();
        model.loadModel(PIXI.loader);
        scene.addGlobalAttribute(ATTR_FACTORY, this);
        scene.addGlobalAttribute(ATTR_MODEL, model);
        this.initializeGame(scene.stage, model);
    }
}