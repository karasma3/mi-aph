import {MyGridMap} from "./MyGridMap";

export class HippoModel {
    // ========================= dynamic data
    score = 0;
    isGameOver = false;
    enemyFireRate = 500;
    map: MyGridMap;
    loadModel(data: any) {

    }

    /**
     * Resets dynamic data
     */
    reset() {
        this.score = 0;
        this.isGameOver = false;
    }
}